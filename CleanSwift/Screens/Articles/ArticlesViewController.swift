//
//  ArticlesViewController.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 13.06.2021.
//

import UIKit

protocol ArticlesDisplayLogic : class {
    func display(data: [ArticleCellModel])
}

class ArticlesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private(set) var router: ArticleRoutingLogic?
    
    private var interactor: ArticlesBusinessLogic?
    private var dataToDisp = [ArticleCellModel]()
    
    weak var cellDelegate: ArticleCellDelegate?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        interactor?.fetchArticles()
        // Do any additional setup after loading the view.
    }
    
    private func setup() {
        let vc = self
        let presenter = ArticlesPresenter()
        let interactor = ArticlesInteractor()
        let router = ArticleRouter()
        router.viewController = vc
        presenter.viewController = vc
        interactor.presenter = presenter
        vc.router = router
        vc.interactor = interactor
    }
    
    private func configTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: .zero)
        cellDelegate = self
    }

}

extension ArticlesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.cellIdent) as? ArticleTableViewCell
        else {return UITableViewCell()}
        cell.setupCell(cellData: dataToDisp[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        cellDelegate?.didTapOnArticle(data: dataToDisp[indexPath.row])
    }
    
    
}

extension ArticlesViewController: ArticleCellDelegate {
    func didTapOnArticle(data: ArticleCellModel) {
        router?.navToDetail(data: data)
    }
}

extension ArticlesViewController: ArticlesDisplayLogic {
    func display(data: [ArticleCellModel]) {
        dataToDisp.removeAll()
        dataToDisp.append(contentsOf: data)
        tableView.reloadData()
    }
}
