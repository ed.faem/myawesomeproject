//
//  UserModel.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation

struct UserModel {
    let userId: Int
    let id: Int
    let title: String
    let body: String
    
}

