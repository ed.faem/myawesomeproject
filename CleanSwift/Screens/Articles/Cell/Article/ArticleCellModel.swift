//
//  ArticleCellModel.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation

struct ArticleCellModel {
    let articleId: Int
    let titleText: String
    let bodyText: String
}
