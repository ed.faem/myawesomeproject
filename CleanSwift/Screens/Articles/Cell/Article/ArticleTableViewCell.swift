//
//  ArticleTableViewCell.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import UIKit

protocol ArticleCellDelegate: class {
    func didTapOnArticle(data: ArticleCellModel)
}

class ArticleTableViewCell: UITableViewCell {
    
    static let cellIdent = "ArticleCell"
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var bodyText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(cellData: ArticleCellModel) {
        titleText.text = cellData.titleText
        bodyText.text = cellData.bodyText
    }
    
}
