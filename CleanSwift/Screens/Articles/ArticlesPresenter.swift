//
//  ArticlesPresenter.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 13.06.2021.
//

import Foundation

protocol ArticlesPresentationLogic {
    func present(data: [UserModel])
}

class ArticlesPresenter {
    weak var viewController: ArticlesDisplayLogic?
}

extension ArticlesPresenter : ArticlesPresentationLogic {
    func present(data: [UserModel]) {
        
        let viewModel = data.map { model -> ArticleCellModel in
            let cell = ArticleCellModel(articleId: Int(model.userId) ?? 0,
                                        titleText: model.title,
                                        bodyText: model.body
            )
            return cell
        }
        viewController?.display(data: viewModel)
    }
}
