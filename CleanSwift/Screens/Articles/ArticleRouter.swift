//
//  ArticleRouter.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation
import UIKit

protocol ArticleRoutingLogic: class {
    func navToDetail(data: ArticleCellModel)
}

class ArticleRouter {
    weak var viewController: UIViewController?
}

extension ArticleRouter: ArticleRoutingLogic {
    func navToDetail(data: ArticleCellModel) {
        let stb = UIStoryboard.init(name: "Details", bundle: nil)
        guard let vc = stb.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return
        }
        vc.router?.dataStore?.userData = data
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
