//
//  ArticlesInteractor.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 13.06.2021.
//

import Foundation

protocol ArticlesBusinessLogic : class {
    func fetchArticles()
}

class ArticlesInteractor {
    var presenter: ArticlesPresentationLogic?
    
    func getUsers(callback: @escaping ([UserModel]?)->()) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        let session = URLSession.shared
        session.dataTask(with: url) { data, response, error in
            if let response = response {
                print("Response - ",response)
            }
            guard let data = data else {return}
            print("Data - ",data)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String:Any]]
                
                let users = json?.map({ user -> UserModel in
                    let userId = user["userId"] as? Int
                    let id = user["id"] as? Int
                    let title = user["title"] as? String
                    let body = user["body"] as? String
                    if let userId = userId, let id = id, let title = title, let body = body {
                        return UserModel(userId: userId, id: id, title: title, body: body)
                    }
                    return UserModel(userId: 1, id: 1, title: "", body: "body")
                })
                callback(users)
                
            } catch {
                print(error)
            }
        }.resume()
    }
    
}

extension ArticlesInteractor : ArticlesBusinessLogic {
    func fetchArticles() {
        
        getUsers(){ [weak self] users in
            DispatchQueue.main.async {
                if let data = users {
                    self?.presenter?.present(data:data)
                }
            }
        }
    }
}
