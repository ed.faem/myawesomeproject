//
//  DetailPresenter.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation

protocol DetailPresentationLogic {
    func present(title: String, text: String)
}

class DetailPresenter {
    weak var viewController: DetailsDisplayLogic?
}

extension DetailPresenter: DetailPresentationLogic {
    func present(title: String, text: String) {
        viewController?.display(title: title, text: text)
    }
    
    
    
    
}
