//
//  DetailsViewController.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import UIKit

protocol DetailsDisplayLogic: class {
    func display(title: String, text: String)
}

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var userTitleLabel: UILabel!
    @IBOutlet weak var userBodyLabel: UILabel!
    
    private(set) var router: (DetailRoutingLogic & DetailDataPassingProtocol)?
    private var interactor: (DetailBusinessLogic & DetailStoreProtocol)?
    
    

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.fetchDetails()
        // Do any additional setup after loading the view.
    }
    
    private func setup() {
        let vc = self
        let interactor = DetailsInteractor()
        let presenter = DetailPresenter()
        let router = DetailRouter()
        presenter.viewController = vc
        interactor.presenter = presenter
        router.dataStore = interactor
        vc.router = router
        vc.interactor = interactor
    }

}

extension DetailsViewController: DetailsDisplayLogic {
    func display(title: String, text: String) {
        userTitleLabel.text = title
        userBodyLabel.text = text
    }
}
