//
//  DetailsInteractor.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation

protocol DetailBusinessLogic {
    func fetchDetails()
}

protocol DetailStoreProtocol: class {
    var userData: ArticleCellModel {get set}
}

class DetailsInteractor: DetailStoreProtocol {
    var userData: ArticleCellModel = ArticleCellModel(articleId: 0, titleText: "", bodyText: "")
    var presenter: DetailPresentationLogic?
    
}

extension DetailsInteractor: DetailBusinessLogic {
    func fetchDetails() {
        presenter?.present(title: userData.titleText, text: userData.bodyText)
    }
    
    
}
