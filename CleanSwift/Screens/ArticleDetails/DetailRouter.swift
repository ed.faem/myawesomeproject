//
//  DetailRouter.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 14.06.2021.
//

import Foundation

protocol DetailRoutingLogic {
    
}

protocol DetailDataPassingProtocol {
    var dataStore: DetailStoreProtocol? {get}
}

class DetailRouter: DetailDataPassingProtocol {
    weak var dataStore: DetailStoreProtocol?
    
    
}

extension DetailRouter: DetailRoutingLogic {
    
}
