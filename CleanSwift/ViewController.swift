//
//  ViewController.swift
//  CleanSwift
//
//  Created by Эдик Бабочиев on 13.06.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func showSpisok(_ sender: UIButton) {
        let stb = UIStoryboard.init(name: "Articles", bundle: nil)
        guard let vc = stb.instantiateViewController(withIdentifier: "ArticlesViewController") as? ArticlesViewController else {
            return
        }
        
        vc.title = "Article"
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

